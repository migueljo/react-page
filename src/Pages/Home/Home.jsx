import React from 'react'
import styles from './Home.module.sass'

const Home = () => (
  <div className={styles.Container}>
    <h1>Home</h1>
  </div>
)

export default Home
